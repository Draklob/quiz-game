﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	public Text questionDisplayText;
	public Text scoreDisplayText;
	public Text timeRemainingDisplayText;
	public SimpleObjectPool answerButtonObjectPool;
	public Transform answerButtonParent;

	public GameObject questionDisplay;
	public GameObject roundEndDisplay;
	public Text highScoreDisplay;

	private DataController dataController;
	private RoundData currentRoundData;
	private QuestionData[] questionPool;

	private bool isRoundActive;
	private float timeRemaining;
	private int questionIndex;
	private int playerScore;
	private List<GameObject> answerButtonGameObjects = new List<GameObject>();

	// Use this for initialization
	void Start () {
		dataController = FindObjectOfType<DataController>();
		currentRoundData = dataController.GetCurrentRoundData();
		questionPool = currentRoundData.questions;
		timeRemaining = currentRoundData.timeLimitInSeconds;
		UpdateTimeRemainingDisplay();

		playerScore = 0;
		questionIndex = 0;

		ShowQuestion();
		isRoundActive = true;
	}
	
	private void ShowQuestion()
	{
		RemoveAnswerButtons();
		// Get the questions of the round.
		QuestionData questionData = questionPool[questionIndex];
		// Set the text of the question.
		questionDisplayText.text = questionData.questionText;

		// Add every answerButton to the pool, set the transform with the parent and set every answer with your respective button.
		for( int i = 0; i < questionData.answers.Length; i++ )
		{
			// Get the established prefab of SimpleOBjectPool.
			GameObject answerButtonGameObject = answerButtonObjectPool.GetObject();
			// Set the GO to the parent.
			answerButtonGameObject.transform.SetParent( answerButtonParent );
			// Add the answeButton to the pool.
			answerButtonGameObjects.Add( answerButtonGameObject );

			// Set reference to the button.
			AnswerButton answerButton = answerButtonGameObject.GetComponent<AnswerButton>();
			// Set text to the button.
			answerButton.Setup( questionData.answers[i]);
		}
	}

	private void RemoveAnswerButtons()
	{
		while( answerButtonGameObjects.Count > 0 )
		{
			answerButtonObjectPool.ReturnObject( answerButtonGameObjects[0]);
			answerButtonGameObjects.RemoveAt(0);
		}
	}

	public void AnswerButtonClicked( bool isCorrect )
	{
		if( isCorrect )
		{
			playerScore += currentRoundData.pointsAddedForCorrectAnswer;
			scoreDisplayText.text = "Score: " + playerScore.ToString();
		}

		if( questionPool.Length > questionIndex + 1 )
		{
			questionIndex++;
			ShowQuestion();
		}
		else
		{
			EndRound();
		}
	}

	public void EndRound()
	{
		isRoundActive = false;
		dataController.SubmitNewPlayerScore( playerScore );
		highScoreDisplay.text = dataController.GetHighestPlayerScore().ToString();
		questionDisplay.SetActive(false);
		roundEndDisplay.SetActive(true);
	}

	public void ReturnToMenu()
	{
		SceneManager.LoadScene("MenuScreen");
	}

	private void UpdateTimeRemainingDisplay()
	{
		timeRemainingDisplayText.text = "Time: " + Mathf.Round( timeRemaining ).ToString();
	}

	private void Update()
	{
		if( isRoundActive )
		{
			timeRemaining -= Time.deltaTime;
			UpdateTimeRemainingDisplay();

			if( timeRemaining <= 0f )
				EndRound();
		}
	}
}
