﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AnswerButton : MonoBehaviour {

	public Text answerText;

	private AnswerData answerData;
	private GameController gameController;

	private void Awake()
	{
		// Establecemos la referencia a través del editor, por eso es publico el "answerText"
		// answerText = GetComponentInChildren<Text>();
		gameController = FindObjectOfType<GameController>();
	}

	public void Setup( AnswerData data )
	{
		answerData = data;
		answerText.text = answerData.answerText;
	}
	
	public void HandleClick()
	{
		gameController.AnswerButtonClicked( answerData.isCorrect );
	}
}
